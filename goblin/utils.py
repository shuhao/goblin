from __future__ import division

from contextlib import contextmanager
from datetime import datetime

import numpy as np
import cv2


def pretty_depth(depth):
  depth = depth / float(np.max(depth)) * (2 ** 16)
  depth = depth.astype(np.uint16)
  depth = cv2.cvtColor(depth, cv2.COLOR_GRAY2BGR)
  return depth


def rgb_to_bgr(video):
  return video[:, :, ::-1]


def float_to_int_tuple(t):
  return tuple([int(i) for i in t])


def draw_square(img, center, r, color, thickness):
  vertex1 = (center[0] - r, center[1] - r)
  vertex2 = (center[0] + r, center[1] + r)
  cv2.rectangle(img, vertex1, vertex2, color, 2)


@contextmanager
def timer(logger, prefix):
  start = datetime.now()
  yield
  delta = datetime.now() - start
  logger.info("{}: {}".format(prefix, delta))
