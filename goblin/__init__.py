from __future__ import absolute_import

import logging

from .eagleeye import EagleEye

logging.basicConfig(format="[%(asctime)s.%(msecs)03d][%(levelname)-.1s] %(message)s", datefmt="%Y-%m-%d %H:%M:%S")


class Goblin(object):
  def __init__(self):
    self.eagle_eye = EagleEye()
    self.eagle_eye.initialize_hardware()

  def run(self):
    self.eagle_eye.run()
