Mystic Might
============

This module consists of code to do mapping and localization. Responsibilities 
are:

 - Given landmark information, map and localize.
