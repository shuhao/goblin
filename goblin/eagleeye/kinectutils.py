
# From http://nicolas.burrus.name/index.php/Research/KinectCalibration
# which is in turn from http://wiki.ros.org/kinect_node/Calibration


def raw_depth_to_meters(raw_depth):
  if raw_depth < 2047:
    return 1.0 / (raw_depth * -0.0030711016 + 3.3309495161)
  return 0


def raw_depth_to_meters2(raw_depth):
  pass


def limit_rolling_window_size(l, size=10):
  while len(l) > size:
    l.pop(0)
