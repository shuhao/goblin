from __future__ import division

import cv2
import numpy as np

from .. import parameters as p


class PickedLandmark(object):
  def __init__(self, id):
    self.id = id
    self.new = True

    self.track_window = None
    self.top_left_corner = None
    self.bottom_right_corner = None
    self.current_center = None
    self.previous_center = None

    self.previous_center_changes = []

  def picked_points(self, picked_points, eagleeye):
    min_x, min_y, max_x, max_y = float("inf"), float("inf"), float("-inf"), float("-inf")

    # Brute force..., because we expect this to be small
    for pt in picked_points:
      if pt[0] < min_x:
        min_x = pt[0]

      if pt[0] > max_x:
        max_x = pt[0]

      if pt[1] < min_y:
        min_y = pt[1]

      if pt[1] > max_y:
        max_y = pt[1]

    self.track_window = (min_x, min_y, max_x - min_x, max_y - min_y)

    self.top_left_corner = (min_x, min_y)
    self.bottom_right_corner = (max_x, max_y)

    self.current_center = self.compute_center()

  def update(self, eagleeye):
    hsv_obj_frame = eagleeye.current_video_hsv[self.top_left_corner[1]:self.bottom_right_corner[1], self.top_left_corner[0]:self.bottom_right_corner[0]]
    mask_obj_frame = eagleeye.current_video_hsv_threshed[self.top_left_corner[1]:self.bottom_right_corner[1], self.top_left_corner[0]:self.bottom_right_corner[0]]
    histogram = cv2.calcHist([hsv_obj_frame], [0], mask_obj_frame, [16], [0, 180])
    # Normalize the histogram such that all values between 0 and 255
    cv2.normalize(histogram, histogram, 0, 255, cv2.NORM_MINMAX)

    # flatten it
    histogram = histogram.reshape(-1)

    prob = cv2.calcBackProject([eagleeye.current_video_hsv, eagleeye.previous_video_hsv], [0], histogram, [0, 180], 1)
    prob &= eagleeye.current_video_hsv_threshed

    term_crit = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 1)
    _, self.track_window = cv2.CamShift(prob, self.track_window, term_crit)
    x, y, w, h = self.track_window
    self.top_left_corner = (x, y)
    self.bottom_right_corner = (x + w, y + h)

    self.previous_center = self.current_center
    self.current_center = self.compute_center()

    # TODO: Maybe use aspect ratio change would be a better idea

    changed_distance = (self.current_center[0] - self.previous_center[0]) ** 2 + (self.current_center[1] - self.previous_center[1]) ** 2
    self.previous_center_changes.append(max(changed_distance, p.CENTER_CHANGES_MINIMUM_DISTANCE))
    if len(self.previous_center_changes) > p.CENTER_CHANGES_TO_KEEP:
      self.previous_center_changes.pop(0)

    if len(self.previous_center_changes) == p.CENTER_CHANGES_TO_KEEP:
      std = max(np.std(self.previous_center_changes[:-p.CENTER_CHANGES_TO_EXAMINE]), p.CENTER_CHANGES_MINIMUM_STD)

      mean = np.mean(self.previous_center_changes[:-p.CENTER_CHANGES_TO_EXAMINE])

      current_mean = np.mean(self.previous_center_changes[-p.CENTER_CHANGES_TO_EXAMINE:])
      z_score = (current_mean - mean) / std
      if z_score >= p.CENTER_CHANGES_IS_ANOMALY_SIGMA:
        return True, (mean, std, current_mean, z_score)

    return False, None

  def center(self):
    return self.current_center

  def compute_center(self):
    return int(round(self.track_window[0] + self.track_window[2] / 2)), int(round(self.track_window[1] + self.track_window[3] / 2))
