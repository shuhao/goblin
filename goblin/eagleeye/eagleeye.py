from __future__ import absolute_import, print_function, division

from datetime import datetime
import math
import logging

import cv2
import cv2.xfeatures2d
import freenect
import numpy as np

from . import parameters as p
from .landmarks.picked_landmark import PickedLandmark
from .. import utils

VIDEO_CV_WINNAME = "VIDEO"
THRESHED_WINNAME = "THRESHED VIDEO"
MAP_VIEWER_WINNAME = "KINECT => 2D MAP"
MAP_VIEWER_WIDTH = 640
MAP_VIEWER_HEIGHT = 480


class KinectError(RuntimeError):
  pass


class EagleEye(object):
  def __init__(self, process_func=None):
    # Initialized stuff
    self.logger = logging.getLogger("EagleEye")
    self.logger.setLevel(logging.DEBUG)
    self.kinect_ctx = None
    self.kinect_dev = None
    self.running = False
    self.process_func = process_func

    # Accounting
    self.current_index = 0
    self.current_video_index = -1
    self.current_depth_index = -1
    self.current_video = None
    self.current_depth = None

    self.current_video_hsv = None
    self.current_video_hsv_threshed = None
    self.previous_video_hsv = None
    self.current_landmark_id = 0

    # More accounting
    self.velocity_reference_landmark = None
    self.previous_velocity_reference_locations = []
    self.current_velocity = np.zeros(3)
    self.landmarks = []
    self.current_selected_points = []

    # User interface
    cv2.namedWindow(VIDEO_CV_WINNAME)
    cv2.namedWindow(THRESHED_WINNAME)
    cv2.namedWindow(MAP_VIEWER_WINNAME)
    cv2.setMouseCallback(VIDEO_CV_WINNAME, self.on_mouse)

  def on_mouse(self, event, x, y, flags, params):
    if event == cv2.EVENT_LBUTTONUP:
      self.add_selected_point(x, y)
    elif event == cv2.EVENT_RBUTTONUP:
      self.remove_last_added_point()

  def get_depth_from_point(self, x, y):
    return int(self.current_depth[int(y), int(x)])

  def add_current_selected_points_to_landmarks(self):
    landmark = PickedLandmark(self.current_landmark_id)
    self.current_landmark_id += 1
    landmark.picked_points(self.current_selected_points, self)
    self.landmarks.append(landmark)
    self.current_selected_points = []
    self.logger.info("locked landmark {}".format(landmark.id))

  def add_selected_point(self, x, y):
    self.current_selected_points.append((x, y))

  def remove_last_added_point(self):
    if len(self.current_selected_points) > 0:
      self.current_selected_points.pop()
    else:
      if len(self.landmarks) > 0:
        self.landmarks.pop()

  def add_velocity_reference_point(self):
    self.velocity_reference_landmark = PickedLandmark(-1)
    self.velocity_reference_landmark.picked_points(self.current_selected_points, self)
    self.current_selected_points = []

  def project_points_to_3d(self, points):
    # Use pinhole camera model for now
    # z is the direction the camera is pointed to
    # Not perfect but pretty good for a demo
    world_3d = []
    for pt in points:
      pt = utils.float_to_int_tuple(pt)
      depth = self.get_depth_from_point(*pt)
      if depth == 0:
        world_3d.append((0, 0, 0))
        continue

      x_in_px = (pt[0] - p.VIDEO_WIDTH // 2)
      y_in_px = (p.VIDEO_HEIGHT // 2 - pt[1])
      if x_in_px == 0:
        z = depth
      else:
        alpha = math.atan(x_in_px / p.KINECT_FX_PX)
        r = abs(x_in_px / math.sin(alpha))
        theta = math.atan(y_in_px / r)
        z = depth * math.cos(theta) * math.cos(alpha)

      world_x = x_in_px * z / p.KINECT_FX_PX
      world_y = y_in_px * z / p.KINECT_FY_PX
      world_z = z

      world_3d.append((world_x, world_y, world_z))

    return world_3d

  def normalize_world_coordinate_for_2d_map(self, world_coordinate):
    # We want the bottom center to be where the camera is
    x = world_coordinate[0] / 4500 * MAP_VIEWER_WIDTH
    z = world_coordinate[2] / 4500 * MAP_VIEWER_HEIGHT

    x += (MAP_VIEWER_WIDTH / 2)
    z = MAP_VIEWER_HEIGHT - z
    return int(round(x)), int(round(z))

  def initialize_hardware(self):
    self.logger.info("initializing underlying hardware (kinect)")
    self.kinect_ctx = freenect.init()
    if self.kinect_ctx is None:
      raise KinectError("cannot initialize kinect ctx")

    try:
      num_devices = freenect.num_devices(self.kinect_ctx)
      self.logger.info("number of devices found: {}, opening {}".format(num_devices, num_devices - 1))

      if num_devices < 1:
        raise KinectError("no device detected! shutting down...")

      self.kinect_dev = freenect.open_device(self.kinect_ctx, num_devices - 1)
      if self.kinect_dev is None:
        raise KinectError("cannot open device")

      freenect.set_depth_callback(self.kinect_dev, self.depth_callback)
      freenect.set_video_callback(self.kinect_dev, self.video_callback)
      freenect.set_video_mode(self.kinect_dev, freenect.RESOLUTION_MEDIUM, freenect.VIDEO_RGB)
      freenect.set_depth_mode(self.kinect_dev, freenect.RESOLUTION_MEDIUM, freenect.DEPTH_REGISTERED)
    except KinectError as e:
      freenect.shutdown(self.kinect_ctx)
      raise e

  def run(self):
    self.running = True
    try:
      freenect.start_depth(self.kinect_dev)
      freenect.start_video(self.kinect_dev)
      while self.running:
        if freenect.process_events(self.kinect_ctx) < 0:
          raise KinectError("failed to process events!")

        self.process()
        if self.process_func is not None:
          self.process_func(self)
    finally:
      freenect.stop_depth(self.kinect_dev)
      freenect.stop_video(self.kinect_dev)
      freenect.close_device(self.kinect_dev)
      freenect.shutdown(self.kinect_ctx)

  def stop(self):
    self.running = False

  def depth_callback(self, device, depth, timestamp):
    # SUCH FRAGILE!
    self.current_depth = depth
    self.current_depth_index += 1

  def video_callback(self, device, video, timestamp):
    self.current_video = video
    self.current_video_index += 1

  def process(self):
    if self.current_video_index < self.current_index or self.current_depth_index < self.current_index:
      # not done yet
      return

    current_time = datetime.now()

    video_ahead, depth_ahead = self.current_video_index - self.current_index, self.current_depth_index - self.current_index
    if video_ahead > 0 or depth_ahead > 0:
      self.logger.warn("Mismatch: video is {} frames ahead, depth is {} frames ahead".format(video_ahead, depth_ahead))

    self.current_video_index = self.current_index
    self.current_depth_index = self.current_index
    self.current_index += 1

    self.current_2d_map = np.zeros((MAP_VIEWER_HEIGHT, MAP_VIEWER_WIDTH, 3), np.uint8)

    self.previous_video_hsv = self.current_video_hsv
    self.current_video_hsv = cv2.cvtColor(utils.rgb_to_bgr(self.current_video), cv2.COLOR_BGR2HSV)
    self.current_video_hsv_threshed = cv2.inRange(self.current_video_hsv, np.array((0., 60., 32.)), np.array((180., 255., 255.)))

    self.do_landmark_detection()
    self.debug_draw_landmark_detection()

    for landmark in self.landmarks:
      landmark.new = False

    if self.velocity_reference_landmark:
      self.track_velocity(current_time)

    cv2.imshow(VIDEO_CV_WINNAME, utils.rgb_to_bgr(self.current_video))
    cv2.imshow(THRESHED_WINNAME, self.current_video_hsv_threshed)
    cv2.imshow(MAP_VIEWER_WINNAME, utils.rgb_to_bgr(self.current_2d_map))
    # cv2.imshow("DEPTH", utils.pretty_depth(self.current_depth))

    keypressed = cv2.waitKey(10)
    if keypressed == 27:
      cv2.destroyAllWindows()
      self.stop()
    elif keypressed == ord("s"):
      self.add_current_selected_points_to_landmarks()
    elif keypressed == ord("u"):
      self.remove_last_added_point()
    elif keypressed == ord("v"):
      self.add_velocity_reference_point()

  def get_landmark_world_coordinate(self, landmark):
    # Just do center for now...
    return self.project_points_to_3d([landmark.center()])[0]
    # points = []
    # for i in xrange(landmark.top_left_corner[0], landmark.bottom_right_corner[0] + 1):
    #   for j in xrange(landmark.top_left_corner[1], landmark.bottom_right_corner[1] + 1):
    #     points.append([i, j])

    # # Gaussian weighted average might be better... but there's not enough time to implement that
    # world_coordinates = self.project_points_to_3d(points)
    # total_x = 0
    # total_y = 0
    # total_z = 0
    # num_valid_points = 0
    # for x, y, z in world_coordinates:
    #   if x == 0 and y == 0 and z == 0:
    #     continue
    #   total_x += x
    #   total_y += y
    #   total_z += z
    #   num_valid_points += 1

    # return total_x / num_valid_points, total_y / num_valid_points, total_z / num_valid_points

  def do_landmark_detection(self):
    # We don't do feature extraction but we do need to find the points that
    # may have moved.
    if len(self.landmarks) <= 0:
      return

    landmarks_to_delete = set([])
    for i, landmark in enumerate(self.landmarks):
      if landmark.new:
        continue

      should_remove, diagnostic = landmark.update(self)
      if should_remove:
        self.logger.info("landmark {} detected an anomaly: {}".format(landmark.id, diagnostic))
        landmarks_to_delete.add(landmark.id)

    self.landmarks = filter(lambda l: l.id not in landmarks_to_delete, self.landmarks)

  def track_velocity(self, current_time):
    # Not really reliable.
    self.velocity_reference_landmark.update(self)

    if len(self.previous_velocity_reference_locations) > 10:
      self.previous_velocity_reference_locations.pop(0)

    if len(self.previous_velocity_reference_locations) == 10:
      self.current_velocity = 0
      for i, l in enumerate(self.previous_velocity_reference_locations):
        if i == 0:
          continue

        # could be cached..
        lprev, tprev = self.previous_velocity_reference_locations[i - 1]
        lnext, tnext = l

        diff = tnext - tprev
        delta_t = diff.seconds * 1000 + diff.microseconds / 1000

        this_velocity = (lnext - lprev) / delta_t / 9  # 9 for 9 velocity values
        self.current_velocity += this_velocity

      self.current_velocity = -self.current_velocity

    location = self.get_landmark_world_coordinate(self.velocity_reference_landmark)[2]
    self.previous_velocity_reference_locations.append((location, current_time))

  def debug_draw_landmark_detection(self):
    current_selected_3d = self.project_points_to_3d(self.current_selected_points)
    cv2.circle(self.current_2d_map, (MAP_VIEWER_WIDTH // 2, MAP_VIEWER_HEIGHT), 15, (255, 0, 0), 2)
    for i, point in enumerate(self.current_selected_points):
      cv2.circle(self.current_video, point, 5, (255, 0, 0), 1)
      cv2.putText(self.current_video, str(self.get_depth_from_point(*point)), point, cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 0))
      map_coordinate = self.normalize_world_coordinate_for_2d_map(current_selected_3d[i])
      cv2.circle(self.current_2d_map, map_coordinate, 5, (255, 255, 255), 1)
      # print(self.get_depth_from_point(*point), math.sqrt(current_selected_3d[i][0]**2 + current_selected_3d[i][1]**2 + current_selected_3d[i][2]**2))

    for landmark in self.landmarks:
      color = p.get_color(landmark.id)
      cv2.rectangle(self.current_video, landmark.top_left_corner, landmark.bottom_right_corner, color)
      cv2.putText(self.current_video, str(landmark.id), landmark.top_left_corner, cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 0))
      cv2.circle(self.current_video, landmark.center(), 5, color, 1)

      world_coordinate = self.get_landmark_world_coordinate(landmark)
      map_coordinate = self.normalize_world_coordinate_for_2d_map(world_coordinate)
      cv2.circle(self.current_2d_map, map_coordinate, 5, color, 2)

    if self.velocity_reference_landmark is not None:
      color = (255, 255, 255)
      cv2.rectangle(self.current_video, self.velocity_reference_landmark.top_left_corner, self.velocity_reference_landmark.bottom_right_corner, color)
      cv2.circle(self.current_video, self.velocity_reference_landmark.center(), 5, color, 1)
      cv2.putText(self.current_video, str(self.current_velocity) + "m/s", self.velocity_reference_landmark.top_left_corner, cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 0))
