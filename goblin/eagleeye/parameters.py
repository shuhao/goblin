
# For landmarks
COLORS = [
  (255, 0, 0),      # BLUE
  (255, 255, 0),
  (0, 255, 0),
  (0, 255, 255),
  (0, 0, 255),
  (255, 0, 255),
  (0, 0, 0),
]


def get_color(i):
  return COLORS[i % len(COLORS)]


VIDEO_WIDTH = 640
VIDEO_HEIGHT = 320
VIDEO_MAX_DIST = VIDEO_WIDTH ** 2 + VIDEO_HEIGHT ** 2

# Focal Length of the Kinect as stated by OpenNI and ROS.
# Sources:
# - http://wiki.ros.org/kinect_calibration/technical
# - http://cmp.felk.cvut.cz/ftp/articles/pajdla/Smisek-CDC4CV-2011.pdf
KINECT_FY_PX = 493
KINECT_FX_PX = 525

# The number of center changes to keep so we can look for anomalies.
CENTER_CHANGES_TO_KEEP = 80

# The last <CENTER_CHANGES_TO_EXAMINE> center changes to look at as an average
# to see if it is an anomaly
CENTER_CHANGES_TO_EXAMINE = 2

# The number of sigma the last <CENTER_CHANGES_TO_EXAMINE> deviates from the
# mean to trigger landmark deletion
CENTER_CHANGES_IS_ANOMALY_SIGMA = 10

# The minimum distance changed to record. Everything below gets bumped to this
CENTER_CHANGES_MINIMUM_DISTANCE = 10

# The minimum standard deviation
CENTER_CHANGES_MINIMUM_STD = 12
