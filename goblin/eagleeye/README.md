Eagle Eye
=========

This module consists of seeing things. Responsibilities are:

 - Getting kinect video/depth feed
 - Finding key points
 - Correlating keypoints between two images (feature extraction)

