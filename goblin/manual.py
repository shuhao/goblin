from __future__ import absolute_import

import logging

import cv2

from .eagleeye import EagleEye


class ManualGoblin(object):
  def __init__(self):
    self.logger = logging.getLogger("manual_goblin")
    self.logger.setLevel(logging.DEBUG)
    self.eagle_eye = EagleEye(self.logger)
    self.eagle_eye.initialize_hardware()

  def step(self):
    self.eagle_eye.flick()

  def run(self):
    try:
      while True:
        self.step()
        while cv2.waitKey(10) == -1:
          pass

    except KeyboardInterrupt:
      pass
    finally:
      self.eagle_eye.shutdown()
