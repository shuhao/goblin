Kinect on Europa
================

This is an exploratory project on potentially using Kinect on a planetary rover, especially on Europa.

The code right now is likely broken as things are still a work in progress.

Requirements
------------

- A Kinect for Xbox 360 or a Kinect 1 sensor
- A Linux computer (other platform probably works too but you're on your own).
  - Specifically, (X)Ubuntu 15.04+
- Python 2.7

We're just going to install everything into the system because virtualenv is way too much of a hassle for this, unfortunately.

Software packages you'll need:

- numpy 1.9.2
- opencv 3.0.0
- `opencv_contrib` 3.0.0
- freenect 0.5.3 (and its python wrapper)

### Some notes ###

If you have a kinect 1437, make sure to build with `redist_package` off for freenect and run `freenect-micview` a couple of times so the correct firmware package gets uploaded. 

Don't plug it back into the Xbox again.

Algorithm Overviews
-------------------

None of these algorithms are meant to be "production ready". They are probably the simplest algorithms I can implement so I can get a proof of concept working. Keep in mind that the proof of concept is to enable a system such as Kinect in a SLAM role, not to have the perfect SLAM system. 

### Landmark detection ###

To detect landmarks, we need to use some simple computer vision trickery. There's nothing fancy to see here. We use OpenCV's SURF keypoint extractor with a FLANN matcher. K means is used to find objects and a small custom algorithm is used to construct the final landmark. This is described as follows:

#### First image taken ####

1. Find all keypoints and descriptors
2. Done

#### Second image taken ####

1. Find all keypoints and descriptors
2. Find matches between 1st and 2nd image descriptors
3. Find k-means between 1st and 2nd image descriptors with labels.
4. For each cluster center, create a landmark based there. Record the 
   image number (for this image: 2) and record the point coordinate and
   the descriptor.
5. Record all well matched descriptor from this image (2nd) and store it as
   a previous good match, along with a reference to the landmark object so
   we can add on to it later.

Third image taken and beyond:

1. Find all keypoints and descriptors
2. Find matches between this image and the previous image's good match 
   descriptors
3. Find k means between the descriptors of this image and the **good match** 
   descriptors of the previous image.
4. For each cluster center found in step 3, find the add the cluster center to
   the landmark object for this image (add point coordinate and descriptor).
5. Try matching this image with previous image (all descriptors).
6. Run k-means on the new image
7. For each cluster center, if in the previous image the cluster center lies in
   a known cluster for that image:
  a. We check all the points matched and see how much the radius of the match
     circle increased by and where the new center is.
  b. We check if any othre points crosses into this new circle.
8. If the cluster center does not lie in the match circle of a previous image, then
  we happily declare this a new landmark, assigning it an id of the largest 
  recorded id + 1.


Problem with this approach:

- No idea if we crossed the same landmark twice.
