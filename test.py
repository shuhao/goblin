import cv2
import cv2.xfeatures2d

import numpy as np

cv2.namedWindow("detector")

img1 = cv2.imread('testdata/middle1.jpg', 0)
img2 = cv2.imread('testdata/left.jpg', 0)

print img1.size

extractor = cv2.xfeatures2d.SURF_create()

keypoint1, descriptor1 = extractor.detectAndCompute(img1, None)
keypoint2, descriptor2 = extractor.detectAndCompute(img2, None)

flann = cv2.FlannBasedMatcher({"algorithm": 0, "trees": 5}, {})
matches = flann.knnMatch(descriptor1, descriptor2, k=2)

matches_mask = [[0, 0] for i in xrange(len(matches))]
points1 = []
points2 = []
for i, (m, n) in enumerate(matches):
  if m.distance < 0.6 * n.distance:
    matches_mask[i] = [1, 0]
    x1, y1 = keypoint1[m.queryIdx].pt
    x2, y2 = keypoint2[m.trainIdx].pt
    x1, y1 = int(round(x1)), int(round(y1))
    x2, y2 = int(round(x2)), int(round(y2))

    points1.append(np.array([x1, y1], dtype=np.float32))
    points2.append(np.array([x2, y2], dtype=np.float32))

points1 = np.array(points1)
points2 = np.array(points2)

# kmeans now
COLORS = [(255, 0, 0), (0, 0, 0)]
term_crit = (cv2.TERM_CRITERIA_EPS, 30, 0.1)
print("p1:", points1)
ret, labels, centers = cv2.kmeans(points1, 2, None, term_crit, 10, 0)
labels = labels.ravel()

good_matches = []
good_matches_masks = []
for i, mm in enumerate(matches_mask):
  if sum(mm) != 0:
    good_matches.append(matches[i])
    good_matches_masks.append([1, 0])

draw_params = {
    "matchColor": (0, 255, 0),
    "singlePointColor": (255, 0, 0),
    "matchesMask": good_matches_masks,
    "flags": 2,
}

import pdb; pdb.set_trace()
img3 = cv2.drawMatchesKnn(img1, keypoint1, img2, keypoint2, good_matches, None, **draw_params)
# add a line between the two images
img3 = np.insert(img3, len(img1[0]), [255, 0, 0], axis=1)

for i, p1 in enumerate(points1):
  p1 = tuple([int(round(j)) for j in p1])
  p2 = tuple([int(round(j)) for j in points2[i]])
  print p1, p2

  # get color from kmeans
  color = COLORS[labels[i]]
  cv2.circle(img3, p1, 15, color, 2)
  p2 = (p2[0] + len(img1[0]) + 1, p2[1])
  cv2.circle(img3, p2, 15, color, 2)

for p in centers:
  p = tuple([int(round(j)) for j in p])
  cv2.circle(img3, p, 30, (0, 0, 255), 4)

cv2.imshow("detector", img3)
while True:
  if cv2.waitKey(10) == 27:
    cv2.destroyAllWindows()
    break
