from __future__ import absolute_import, print_function

import argparse
import os.path
import sys

import cv2

from goblin.eagleeye import landmarks

CV_LOAD_IMAGE_GRAYSCALE = 0

if __name__ != "__main__":
  raise RuntimeError("cannot import this module!")


def try_parsing_arguments():
  parser = argparse.ArgumentParser(description="eagle eye testing program")
  parser.add_argument("--algorithm", default="default", choices=landmarks.algorithms.keys(), help="algorithms to use for landmark extraction")
  parser.add_argument("images", nargs="+", help="images in chronological order")
  args = parser.parse_args()

  for image in args.images:
    if not os.path.isfile(image):
      print("error: {} is not a valid file!".format(image), file=sys.stderr)
      parser.print_usage()
      sys.exit(1)

  return args, parser


def main():
  args, parser = try_parsing_arguments()
  detector = landmarks.algorithms[args.algorithm]()
  for image_path in args.images:
    img = cv2.imread(image_path, CV_LOAD_IMAGE_GRAYSCALE)
    detector.feed(img)

  while True:
    if cv2.waitKey(10) == 27:
      cv2.destroyAllWindows()
      break

if __name__ == "__main__":
  main()
