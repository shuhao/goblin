#!/bin/bash

echo "This is just a suggested setup script on Ubuntu."
echo "Review its content to make sure it is not going to screw up your system."
echo
if [ "$MY_WARRENTY_HAS_BEEN_VOIDED" != "1" ]; then 
  # You know what to do.
  exit 1
fi

# Take advantage of sudo caching :D
sudo echo "acquired root"

echo "Your (lack of) warrenty has now been voided. You have 5s to abort if you want"
sleep 5

set -xe

# Ubuntu specific!
sudo apt-get install -y git cmake build-essential libusb-1.0-0-dev freeglut3-dev libxmu-dev libxi-dev libgtk2.0-dev pkg-config python-dev

sudo pip install numpy

mkdir -p dep
cd dep

wget -O opencv-3.0.0.tar.gz https://github.com/Itseez/opencv/archive/3.0.0.tar.gz
wget -O opencv_contrib-3.0.0.tar.gz https://github.com/Itseez/opencv_contrib/archive/3.0.0.tar.gz
wget -O freenect-0.5.3.tar.gz https://github.com/OpenKinect/libfreenect/archive/v0.5.3.tar.gz

tar xvzf *.tar.gz

# lol
CORECOUNT=`grep MHz /proc/cpuinfo | wc -l`

pushd opencv
  mkdir -p build
  pushd build
    cmake -DOPENCV_EXTRA_MODULES_PATH=../../opencv_contrib-3.0.0/modules ..
    make -j${CORECOUNT} 
    sudo make install
  popd
popd

pushd libfreenect-0.5.3
  mkdir -p build 
  pushd build
    # Yes. we are going to make it apt installable
    cmake -L .. -DBUILD_REDIST_PACKAGE=OFF -DBUILD_CPACK_DEB=ON
    make 
    cpack
    sudo dpkg -i libfreenect-dev-0.5-x86_64.deb
  popd
  sudo cp platform/linux/udev/51-kinect.rules /etc/udev/rules.d
  pushd wrappers/python
    sudo python setup.py install
  popd
popd

